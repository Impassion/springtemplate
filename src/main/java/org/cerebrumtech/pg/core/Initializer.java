package org.cerebrumtech.pg.core;

import org.apache.log4j.Logger;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.HashSet;
import java.util.Set;

public class Initializer implements WebApplicationInitializer {
    private static Logger log = Logger.getLogger(Initializer.class);
    private Set<String> mappingConflicts = new HashSet<String>();

    @Override
    public void onStartup(javax.servlet.ServletContext container) throws ServletException {
        log.info("Inside application initializer...");

        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(AppConfig.class);
        container.addListener(new ContextLoaderListener(rootContext));

        log.info("Loading main servlet:");
        AnnotationConfigWebApplicationContext mainServletContext = new AnnotationConfigWebApplicationContext();
        mainServletContext.register(MainServlet.class);

        ServletRegistration.Dynamic main = container.addServlet("main", new DispatcherServlet(mainServletContext));
        main.setLoadOnStartup(1);
        mappingConflicts.addAll(main.addMapping("/"));

        log.info("Main servlet loaded.");



        if (!mappingConflicts.isEmpty()) {
            for (String s : mappingConflicts) {
                log.error("Mapping conflict: " + s);
            }
            throw new IllegalStateException(
                    "Application cannot been initialized cause of mapping conflicts.");
        }
    }
}
